<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

if( config('app.env') == "local")
{
    Route::get('/logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
}


Route::match(array('GET','POST'),'/', 'PageController@index');
// Route::get('/', 'PageController@index');
Route::get('/{slug}', 'PageController@slug');

Route::get('/home2', 'PageController@index');
