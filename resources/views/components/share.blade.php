<section id="share">
   <div class="container tac">
      <img src="/img/share-ope.gif" class="gif" alt="">
      <p style="color:#000" class="m-t-20 karla">
         Partager cette sélection de Noël à vos amis Grands-Parents
      </p>
      <div class="row share-popup m-t-40">
         <div class="col-2 border-right offset-3">
            <a href="{!! getsharelink("email", url('/')) !!}"><i class="fas fa-envelope"></i></a>
         </div>
         <div class="col-2 border-right">
            <a href="{!! getsharelink("facebook", url('/')) !!}"><i class="fab fa-facebook-square"></i></a>
         </div>
         <div class="col-2">
            <a href="{!! getsharelink("whatsapp", url('/')) !!}"><i class="fab fa-whatsapp"></i></a>
         </div>
      </div>
   </div>
</section>