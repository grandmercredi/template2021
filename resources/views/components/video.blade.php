<section id="video" class="p-b-100">
   <div class="container">
      <div class="tac m-b-50">
         <h2>
           <span style="background: linear-gradient(wheat,wheat) bottom/100% 45% no-repeat;">Video.js</span>
         </h2>
      </div>
      <p>
         Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum maiores quaerat praesentium veritatis vitae possimus 
         sit a recusandae, quibusdam, quia harum voluptatibus reiciendis cum itaque neque, deleniti ab enim? Debitis.
      </p>
      <video
         id="my-video"
         class="video-js vjs-big-play-centered m-t-40 vjs-16-9"
         controls
         preload="auto"
         poster="/img/bg-video.png"
         data-setup="{}">
         <source src="https://grand-mercredi-subdomains.s3.eu-west-3.amazonaws.com/Disney/DOWNLOAD+TO+GO+25sec+V1.mp4" type="video/mp4" />
      </video>
   </div>
</section>