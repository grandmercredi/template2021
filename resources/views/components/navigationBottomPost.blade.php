<div class="post-navigation m-t-40 m-b-20">
    <a href="/{{$article->prev->slug}}" class="post-prev">
        <div class="post-prev-title"><span>Article précédent</span>{{$article->prev->title}}</div>
    </a>
    <a href="/#articles" class="post-all"><span class="gotohome">Accueil</span><div class="icon-holder"><i class="fa fa-home"></i><span class="text-mutted fz12"></span></div></a>
    <a href="/{{$article->next->slug}}" class="post-next">
        <div class="post-next-title"><span>Article suivant</span>{{$article->next->title}}</div>
    </a>
</div>
