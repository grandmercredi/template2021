<div class="social-icons social-icons-colored">
    <ul>
        <li class="social-facebook">
            <a href="{!! getsharelink("facebook", url('/').'/'.$article->slug) !!}"><i class="fab fa-facebook"></i></a>
        </li>
        <li class="social-whatsapp">
            <a href="{!! getsharelink("whatsapp", url('/').'/'.$article->slug) !!}"><i class="fab fa-whatsapp"></i></a>
        </li>
        <li class="social-twitter">
            <a href="{!! getsharelink("twitter", url('/').'/'.$article->slug) !!}"><i class="fab fa-twitter"></i></a>
        </li>
        <li class="social-linkedin">
            <a href="{!! getsharelink("linkedin", url('/').'/'.$article->slug) !!}"><i class="fab fa-linkedin"></i></a>
        </li>
    </ul>
</div>
