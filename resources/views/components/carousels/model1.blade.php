<section id="carousel" class="model1" style="padding-bottom:0">
   <div class="container-fullscreen">
      <h2 class="title tac m-t-50 m-b-50">
         Carousel title
      </h2>

      <!-- Carousel Filter -->
      <nav class="grid-filter gf-outline tac m-b-60 " data-layout="#portfolio-carousel">
         <ul>
            @foreach( $filters_1 as $f => $c )
               <li><a href="#" style="--color:{{ $c }}" class="nav-filters" id="nav-filter-{{ $loop->iteration }}" data-category=".filter_{{ Str::slug($f) }}">{{ $f }}</a></li>
            @endforeach
         </ul>
      </nav>
      <!-- end: Carousel Filter -->

      <!-- Carousel -->
      <div id="portfolio-carousel" class="grid-layout">
      @foreach ($carousel1 as $c)
      
         <div class="portfolio-item @foreach($c->tags as $tag) {{"filter_".Str::slug($tag)}} @endforeach" style="background: {{ $c->bg }}">
            <h2 class="item-title m-t-50 m-b-50 tac">
               {{ $c->title }}
            </h2>
            
            <div class="carousel team-members team-members-shadow container m-t-20" data-items="1">
                
               @foreach ($c->elem as $a)
                <div>
                   <div class="card-carousel position-relative">
                     <div class="container row align">
                        <div class="col-lg-6">
                           <a href="{{ $a['link'] }}" target="_blank">
                              <source srcset="{{ substr($a['img_index'],0,-4).".webp" }}" type="image/webp">
                              <source srcset="{{ substr($a['img_index'],0,-4).".jpg" }}" type="image/jpeg">
                              <img src="{{ $a['img_index'] }}">
                           </a>
                        </div>
                        <div class="col-lg-6 position-relative">
                           <div class="align">
                              <h3 style="color: {{ $c->color_1 }};font-weight:700;">
                                 {{ $a['title_1'] }}
                              </h3>
                              <p style="color: {{ $c->color_1 }}">
                                 {{ $a['text_1'] }}
                              </p>
                              <div class="card-avis" style="background-color: {{ $c->color_bg }}">
                                 <h3 class="kalam" style="color: {{ $c->color_2 }};font-weight:700;">
                                    {{ $a['title_2'] }}
                                 </h3>
                                 <p style="color: {{ $c->color_1 }}">
                                    {{ $a['text_2'] }}
                                 </p>
                              </div>
                           </div>

                        </div>
                      </div>
                      <div class="tac hideondesktop-lg m-t-20">
                      <a href="{{ $a['link'] }}" class="btn btn-univers-2" style="--color: {{ $c->color_1 }}" target="_blank">en savoir plus</a>
                      </div>
                   </div>
   
                   <div class="tac hideonmobile-lg m-t-20">
                     <a href="{{ $a['link'] }}" class="btn btn-univers" style="--color: {{ $c->color_1 }}" target="_blank">en savoir plus</a>
                   </div>
                </div>
                @endforeach
            
            </div>
         </div>
         
      @endforeach
   </div>
   <!-- end carousel -->

   </div>
</section>