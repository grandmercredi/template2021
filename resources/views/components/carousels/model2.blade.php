<section id="carousel" class="model2">
   <div class="container">

      <div class="heading-text heading-line text-center">
         <h4 class="title">Carousel title</h4>
      </div>
      
      <div class="carousel team-members team-members-shadow" data-items="3">
          @foreach ($carousel2 as $a)
          
          <div class="team-member">
            <h3 style="color:#E41124;min-height: 74px;" class="delius">{{ $a->title }}</h3>
               <div class="team-image">
                  <a href="{{ $a->link }}" target="_blank">
                   <source srcset="{{ substr($a->img_index,0,-4).".webp" }}" type="image/webp">
                   <source srcset="{{ substr($a->img_index,0,-4).".jpg" }}" type="image/jpeg">
                   <img src="{{ $a->img_index }}" alt="{{ $a->title}}">
                  </a>
               </div>
               <div class="team-desc">
                  <p style="min-height:96px">{!! $a->description !!}</p>
                  <a href="{{ $a->link }}" class="btn btn-rounded" style="--color:#E41124" target="_blank">En savoir plus</a>
               </div>
          </div>
   
          @endforeach
      </div>
   </div>
</section>