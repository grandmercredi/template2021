<div id="slider" class="inspiro-slider slider-fullscreen position-relative">
   
    <div class="wrap-deck t-side">
        <p class="tac" style="margin:auto;">
            <a href="https://grand-mercredi.com" target="_blank">
                <img src="/img/logo-gm-slider.png" class="logo-gm-slider" alt="Logo GM">
            </a>
            <span class="logo-sep">&</span>
            <a href="https://bit.ly/3l0LNAy" target="_blank">
                <img src="/img/logo-annonceur.png" class="logo-annonceur-slider" alt="Logo Annonceur">
            </a>
            <br/>
            <span class="introduce">présentent</span>
        </p>
    </div>

    <div class="container tac">
        <img src="/img/logo-ope-slider.png" class="img-fluid hideonmobile-lg align-vertical" alt="Logo Opé Desktop">
        <img src="/img/logo-ope-slider-mob.png" class="img-fluid hideondesktop-lg align-vertical" alt="Logo Opé Mobile">
    </div>

   @foreach ($sliders as $a)
   <div class="slide kenburns" style="background-image:url('{{ $a->img_bg }}');">
       <div class="bg-overlay"></div>
       <div class="container">
           <div class="slide-captions text-center m-t-100">
                

                {{-- Just to show others sliders with one click from the page should be removed--}}
                <a href="/?slider=imgBackground" data-animate="fadeInUp" data-animate-delay="1200" class="btn btn-rounded" style="{{ $a->color }}">Slider imgBackground</a>
                <a href="/?slider=parallax" data-animate="fadeInUp" data-animate-delay="1500" class="btn btn-square" style="{{ $a->color }}">Slider Parallax</a>
                <a href="/?slider=video" data-animate="fadeInUp" data-animate-delay="1800" class="btn btn-corner-rounded" style="{{ $a->color }}">Slider Video</a>
                {{--------}}

           </div>
       </div>

    </div>
    @endforeach

    <div class="scrolldown-animation" id="scroll-down">
        <a class="scroll-to" href="#manifesto">
            <img src="/images/scrolldown-arrow.png" alt="Scrolldown Arrow" style="width:16px;height:69px;">
        </a>
    </div>

</div>
