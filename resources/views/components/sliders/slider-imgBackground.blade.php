<section id="slider" class="fullscreen imgBackground">
   <div class="container tac">

      <div class="wrap-deck t-side">
         <p class="tac" style="margin:auto;">
            <a href="https://grand-mercredi.com" target="_blank">
               <img src="/img/logo-gm-slider.png" class="logo-gm-slider" alt="Logo GM">
            </a>
            <span class="logo-sep">&</span>
            <a href="https://bit.ly/3l0LNAy" target="_blank">
               <img src="/img/logo-annonceur.png" class="logo-annonceur-slider" alt="Logo Annonceur">
            </a>
            <br/>
            <span class="introduce">présentent</span>
         </p>
      </div>

      <img src="/img/logo-ope-slider.png" class="img-fluid hideonmobile-lg" alt="Logo Opé Desktop">
      <img src="/img/logo-ope-slider-mob.png" class="img-fluid hideondesktop-lg" alt="Logo Opé Mobile">

      <div class="m-t-50">
         {{-- Just to show others sliders with one click from the page should be removed--}}              
         <a href="/?slider=multy" data-animate="fadeInUp" data-animate-delay="1200" class="btn btn-rounded" style="--color:#e41124">Slider Multy</a>
         <a href="/?slider=parallax" data-animate="fadeInUp" data-animate-delay="1500" class="btn btn-square" style="--color:#7fa9d1">Slider Parallax</a>
         <a href="/?slider=video" data-animate="fadeInUp" data-animate-delay="1800" class="btn btn-corner-rounded" style="--color:#075a4a">Slider Video</a>
         {{--------}}
     </div>

   </div>
   <div class="scrolldown-animation" id="scroll-down">
      <a class="scroll-to" href="#manifesto">
          <img src="/images/scrolldown-arrow.png" alt="Scrolldown Arrow">
      </a>
  </div>
</section>