<section id="portfolio" class="model1">
    <div class="container">

                <h2 class="title tal m-t-50 m-b-50">
                    Portfolio title
                </h2>

                <!-- Portfolio Filter -->
                <nav class="grid-filter gf-outline" data-layout="#portfolio-div">
                    <ul>
                        @foreach( $filters_2 as $f => $c )
                        <li><a href="#" style="--color:{{ $c }}" class="nav-filters" id="nav-filter-{{ $loop->iteration }}" data-category=".filter_{{ Str::slug($f) }}">{{ $f }}</a></li>
                        @endforeach
                    </ul>
                    <div class="grid-active-title">{{ array_key_first($filters_2) }}</div>
        
                </nav>
                <!-- end: Portfolio Filter -->
        
                <!-- Portfolio -->
                <div id="portfolio-div" class="grid-layout portfolio-2-columns" data-margin="50">
                    @foreach( $portfolio1 as $a )
                    <!-- portfolio item -->
                    <div class="portfolio-item no-overlay m-b-80 @foreach($a->tags as $c) {{ "filter_".Str::slug($c) }} @endforeach">
                        <div class="portfolio-item-wrap">
                            <div class="portfolio-image home">
                                <p style="--color:{{ $a->color1 }}" class="text-border m-img tal title-width">{!! $a->title !!}</p>
                                <a href="{{ $a->link }}" @if (isset($a->target)) target="_blank" @endif>
                                    <img style="--color:{{ $a->color2 }}" class="portfolio-img-width shadow" src="{{ $a->img_index }}">
                                </a>
                            </div>
                            <div class="square tal" style="--color:{{ $a->color2 }}">
                                <a href="{{ $a->link }}" style="--color:{{ $a->color1 }}" class="btn" @if (isset($a->target)) target="_blank" @endif>{!! $a->title !!}</a>
                                <p>{{ $a->description }}</p>
                            </div>
                        </div>
                    </div>
                    <!-- end: portfolio item -->
                    @endforeach
        
                </div>
                <!-- end: Portfolio -->
        </div>
    <img class="yellow-vie-img hideonmobile" src="/img/vdf-yellow.png">
</section>