<section id="portfolio">
    <div class="container">
        <div class="heading-text heading-line text-center">
            <h4 style="font-size:35px;">Portfolio</h4>
         </div>
         
        <!-- Portfolio Filter -->
        <nav class="grid-filter gf-outline" data-layout="#portfolio-div">
            <ul>
                <li class="active"><a href="#" data-category="*">Tous</a></li>
                @foreach( $filters as $f )
                    <li><a href="#" class="{{ $f }}" data-category=".filter_{{ Str::slug($f) }}">{{ $f }}</a></li>
                @endforeach
            </ul>
            <div class="grid-active-title">Tous</div>

        </nav>
        <!-- end: Portfolio Filter -->

        <!-- Portfolio -->
        <div id="portfolio-div" class="grid-layout portfolio-3-columns" data-margin="20">
            @foreach( $articles as $a )
            @php

                if( property_exists($a, 'type') and $a->type=="link")
                        $slug = $a->slug;
                else    $slug = "/".$a->slug;

            @endphp
            <!-- portfolio item -->
            <div class="portfolio-item no-overlay img-zoom  @foreach($a->tags as $c) {{ "filter_".Str::slug($c) }} @endforeach">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <a href="{{ $slug }}" target="{{ $a->target ?? ''}}">
                            <picture>
                                <source srcset="{{ substr($a->img_index,0,-4).".webp" }}" type="image/webp">
                                <source srcset="{{ substr($a->img_index,0,-4).".jpg" }}" type="image/jpeg">
                                <img src="{{ $a->img_index }}" alt="{{ $a->title}}">
                            </picture>
                            @if(in_array("Videos",$a->tags))
                                <button type="button" class="btn btn-dark iconovercard bg_kd_red">
                                    <i class="fa fa-play"></i>
                                </button>
                            @endif
                        </a>
                    </div>
                    <div class="portfolio-description">
                        <a href="{{ $slug }}" target="{{ $a->target ?? ''}}">
                            <h3>{{ $a->title }}</h3>
                            <p>{!! $a->description !!}</p>
                        </a>
                    </div>
                </div>
            </div>
            <!-- end: portfolio item -->
            @endforeach

        </div>
        <!-- end: Portfolio -->
    </div>
</section>
