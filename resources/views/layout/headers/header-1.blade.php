<!-- Header -->
<header id="header" class="dark submenu-light" data-fullwidth="true" data-transparent="true">
   <div class="header-inner">
       <div class="container">
          <div class="row">
            <div class="col-lg-8 offset-lg-2">
                  <!--Logo-->
                  <div id="logo" class="position-relative">
                     <a href="/">
                        <img src="/img/logo-ope.png" alt="Logo Ope" class="logo-ope">
                     </a>
               </div>
               <!--End: Logo-->
               <!--Navigation Resposnive Trigger-->
               <div id="mainMenu-trigger">
                     <a class="lines-button x"> <span class="lines"></span> </a>
               </div>
               <!--end: Navigation Resposnive Trigger-->
               <!--Navigation-->
               <div id="mainMenu">
                     <div class="container">
                        <nav>
                           <ul>
                                 <li><a href="#portfolio-carousel">Sélection de Noël</a></li>
                                 <li><a href="#testeur">Petits-Enfants testeurs</a></li>
                           </ul>
                        </nav>
                     </div>
               </div>
               <!--end: Navigation-->
            </div>
          </div>
       </div>
   </div>
 </header>
 <!-- end: Header -->