<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NWNT6R4');</script>
<!-- End Google Tag Manager -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="author" content="{{ $article->author ?? 'Grand-Mercredi'}}" />
<meta name="description" content="{{ $article->description ?? ''}}"/>
<title>{{ $article->title ?? ''}}</title>

 <!-- Favicons-->
 <link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-touch-icon.png">
 <link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png">
 <link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png">
 <link rel="manifest" href="/favicons/site.webmanifest">

 <!-- social graph -->
<meta property="og:title" content="{{ $article->seo_title ?? $article->title ?? ''}}"/>
<meta property="og:type" content="{{ $article->seo_type ?? 'website'}}"/>
<meta property="og:url" content="{!! $article->seo_url ?? url()->current() !!}"/>
<meta property="og:image" content="{{url('/')}}{{$article->seo_img ?? $article->img_post ?? $article->img_index ?? '/img/.jpg'}}"/>
<meta property="og:locale" content="fr_FR"/>
<meta property="og:description" content="{{ $article->seo_description ?? $article->description ?? ''}}"/>
<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="" />
<meta name="twitter:title" content="{{ $article->seo_title ?? $article->title ?? ''}}"/>
<meta name="twitter:image" content="{{ $article->seo_img ?? $article->img_post ?? $article->img_index ?? url('/').'/img/.jpg'}}"/>
<meta name="twitter:description" content="{{ $article->seo_description ?? $article->description ?? ''}}" />

@yield('meta')

<!-- Stylesheets & Fonts -->
<link href="https://fonts.googleapis.com/css?family=Jacques+Francois+Shadow|Poppins|Rozha+One|Montserrat:ital,wght@0,400;0,500;0,700;0,800;0,900;1,700;1,800;1,900&display=swap" rel="stylesheet">

<link href="/css/bootstrap.css" rel="stylesheet">
<link href="/css/theme.css" rel="stylesheet">
<link href="{{ mix('/css/custom.css') }}" rel="stylesheet">


@yield('styles')

<!-- Video.js -->
<link href="https://vjs.zencdn.net/7.8.4/video-js.css" rel="stylesheet" />

<!-- font-awesome -->
<script src="https://kit.fontawesome.com/f332aca5b4.js" crossorigin="anonymous"></script>