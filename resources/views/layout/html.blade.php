<!DOCTYPE html>

<html lang="fr">
<head>
    @include('layout.head')
</head>

<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NWNT6R4"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <!-- Body Inner -->
    <div class="body-inner">
        
        <!--HEADER-->
        @yield('header')
        <!--END: HEADER-->
        
        <!--CONTENT-->
        @yield( 'content')
        <!--END: CONTENT-->
        
        <!-- FOOTER -->
        @include('layout.footer')
        <!-- end: FOOTER -->
    
    </div>
    <!-- end: Body Inner -->

    <!-- Scroll top -->
    <a id="scrollTop"><i class="icon-chevron-up"></i><i class="icon-chevron-up"></i></a>

    <!--hack IE11-->
    <script src="/js/polyfill.min.js"></script>
    
    <!--Vendor-->
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap.js"></script>

    <!-- Video.js -->
    <script src="https://vjs.zencdn.net/7.8.4/video.js" defer></script>

    <!--Template functions-->
    <script src="/js/theme.js"></script>

    <!--Custom functions-->
    <script src="{{ mix('/js/custom.js') }}" defer></script>    

    @yield('scripts')
</body>
</html>
