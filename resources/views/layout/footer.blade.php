<footer id="footer">
   <div class="container-fluid">
        <div class="row">

            <div class="col-md-3 position-relative">
                <div class="tac">
                    <a href="https://www.grand-mercredi.com" target="_blank">
                        <img class="gm-footer-logo m-b-20" src="/img/logo-gm-footer.png" />
                    </a>
                    <p class="text">
                        Le site <br>
                        des Grands-Parents <br>
                        d'aujourd'hui
                    </p>
                </div>
            </div>

            <div class="col-md-6 position-relative">
                <div class="centered tac share">
                    <h4>
                        <span class="share-title">
                            Rejoignez-nous
                        </span>
                    </h4>
                    <div class="share-icons">
                        <a href="https://www.instagram.com/grandmercredi" target="_blank"><i class="fab fa-instagram"></i></a>
                        <a href="https://www.youtube.com/channel/UCZRMQ7L3z5Fuc0WQlLTNcPA/featured" target="_blank"><i class="fab fa-youtube"></i></a>
                        <a href="http://facebook.com/grandmercredi"><i class="fab fa-facebook"></i></a>
                        <a href="https://www.linkedin.com/company/grand-mercredi/" target="_blank"><i class="fab fa-linkedin"></i></a>
                    </div>
                </div>
            </div>

            <div class="col-md-3 tac">
                <div class="centered tac">
                    <p class="text">
                        Une opération <br>
                        pensée avec coeur <br>
                        par Grand-Mercredi <br>
                        et ANNONCEUR
                    </p>
                </div>
            </div>

        </div>
   </div>
</footer>
