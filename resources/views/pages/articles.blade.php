@extends(config('ZT.theme_views').'.layout.html')

@section('content')
   <section id="article" class="">
      <div class="container">

         <!-- Post -->
         <div id="blog" class="single-post col-lg-11 center">

            <!-- Post single item-->
            <div class="post-item">
               <div class="post-item-wrap">

                  @if( isset($article->video) and $article->video )
                     <div class="post-video">
                        <iframe width="560" height="315" src="{{ $article->video }}" frameborder="0" allowfullscreen></iframe>
                     </div>

                  @elseif( isset($article->img_post))
                     <div class="post-image">
                        <picture>
                           <source srcset="{{ substr($article->img_post,0,-4).".webp" }}" type="image/webp">
                           <source srcset="{{ substr($article->img_post,0,-4).".jpg" }}" type="image/jpeg">
                           <img src="{{ $article->img_post }}" alt="{!! $article->title!!}">
                        </picture>
                     </div>
                  @endif

                  <div class="post-item-description">
                     <h1>{!! $article->title !!}</h1>

                     <div class="post-meta m-b-40 ">
                        <span class="post-meta-description">
                           {!! $article->description !!}
                        </span>
                        <span class="post-meta-share">
                           @include('components.articleShare')
                        </span>
                     </div>
                     
                     <section id="article-content">
                        @if( isset($article->view) and $article->view ) @include( "pages.articles.".$article->view ) @endif
                     </section>
                  </div>

                  @if( ! isset ($ajax) or ! $ajax)  @include('components.navigationBottomPost') @endif
               </div>
                <!-- end: Post single item-->

            </div>
            <!-- articles relatifs -->
            <!-- fin articles relatifs -->
         </div>
         <!-- end: blog -->
      </div>
   </section>
@endsection