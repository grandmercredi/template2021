<?php
return [
   /*
   |--------------------------------------------------------------------------
   | Les URLs pour partager
   |--------------------------------------------------------------------------
   */

   'preview_token' => "ezj6vaqodle4vrevar7a575r",    // ?gmtoken=ezj6vaqodle4vrevar7a575r

  'sharing' => [
    'facebook' => [
        'uri' => 'https://www.facebook.com/sharer/sharer.php?u=',
    ],
    'twitter' => [
        'uri' => 'https://twitter.com/intent/tweet?text=',
    ],
    'email' => [
        'uri' => 'mailto:?body=',
    ],
    'linkedin' => [
        'uri' => 'https://www.linkedin.com/shareArticle?mini=true&url='
    ],
    'whatsapp' => [
        'uri' => 'https://wa.me/?text='
    ]
],

];
