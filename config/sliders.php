<?php
return [
    /*
   |--------------------------------------------------------------------------
   | Les filtres du blog
   |--------------------------------------------------------------------------
   */
    'filters' => ['Test #1', 'Test #2', 'Test #3'],
    /*
   |--------------------------------------------------------------------------
   | Les articles du site
   |--------------------------------------------------------------------------
   */

   'sliders' => [
      [
         'title'        => 'Slider',
         'desc'         => 'Explore the new world of creativity',
         'img_bg'       => '/img/slider-1.jpg',
         'color'        => '#000'
      ],
      [
         'title'        => 'Slider',
         'desc'         => 'Explore the new world of creativity',
         'img_bg'       => '/img/slider-2.jpg',
         'color'        => '#fff'
      ]
   ]
];