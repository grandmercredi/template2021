const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.styles(
[
    'resources/css/style.css',
    'resources/css/responsive.css',
    'resources/css/theme.css',
    'ressources/css/rtl.css'
],
'public/css/theme.css');

mix.styles(
[
    'resources/css/custom.css'
],
'public/css/custom.css');

//'public/css/custom.css').options({  postCss: [ require('postcss-css-variables')()]});

mix.scripts(
[
    'resources/js/functions.js'
],
'public/js/theme.js');


mix.scripts(
[
   'resources/js/share.js',
   'resources/js/custom.js'
],
'public/js/custom.js');


if (mix.inProduction())
{
    mix.version([ 'public/css/custom.css', 'public/js/custom.js']);
}

mix.copy('resources/js/jquery.js', 'public/js/jquery.js');
mix.copy('resources/js/plugins.js', 'public/js/bootstrap.js');
mix.copy('resources/js/polyfill.min.js', 'public/js/polyfill.min.js');
mix.copy('resources/js/modernizr-custom.js', 'public/js/modernizr-custom.js');

//mix.copy('resources/js/picturefill.min.js', 'public/js/picturefill.min.js');

mix.copy('resources/css/plugins.css', 'public/css/bootstrap.css');
mix.copyDirectory('resources/assets/webfonts', 'public/webfonts');
mix.copyDirectory('resources/assets/images', 'public/images');
mix.copyDirectory('resources/assets/img_optim', 'public/img');
mix.copyDirectory('resources/assets/favicons', 'public/favicons');

// mix.copyDirectory('resources/assets/img/diy_jardiniere', 'public/img/diy_jardiniere');
// mix.copyDirectory('resources/assets/img/diy_oiseaux', 'public/img/diy_oiseaux');
// mix.copyDirectory('resources/assets/img/diy_marrons', 'public/img/diy_marrons');
// mix.copyDirectory('resources/assets/img/diy_recuperateur', 'public/img/diy_recuperateur');
// mix.copyDirectory('resources/assets/img/diy_pate', 'public/img/diy_pate');
// mix.copyDirectory('resources/assets/img/diy_moulin', 'public/img/diy_moulin');

// mix.browserSync('local.kinder');
