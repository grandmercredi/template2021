<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class PageController extends Controller
{

    private function prepareItems( $items, $token = false )
    {
        $newitems = [];
        $preview = true;
        
        foreach( $items as $i )
        {
            if( ! array_key_exists('slug', $i )) $i['slug'] = \Str::slug( $i['title'] );

            if( ! array_key_exists('preview', $i ) or $i['preview'] == false or ($i['preview'] == true  and $token == $preview ))
            $newitems[] = ( object ) $i;

        };
        return $newitems;
    }


    private function getRelatifs( $article, $articles )
    {
       $relatifs1 = [];
       $relatifs2 = [];
       $relatifs3 = [];

       foreach( $articles as $k =>$a )
       {
           if( $a->title == $article->title) continue;
           if( substr($a->slug,0,4) == "http") continue;
           if(  array_key_exists(0, $a->tags ) and array_key_exists(0, $article->tags ) and $a->tags[0] == $article->tags[0] and count( $relatifs1 ) < 3)
           {
                $relatifs1[$k] = $a;
           }

           if(  array_key_exists(1, $a->tags ) and array_key_exists(1, $article->tags ) and ! array_key_exists( $k, $relatifs1)
                and $a->tags[1] == $article->tags[1] and count( $relatifs1 ) < 3 and count( $relatifs2 ) < 3)
           {
                $relatifs2[$k] = $a;
           }

           if(  count( $relatifs1 ) < 3 and count( $relatifs2 ) < 3 and count( $relatifs3 ) < 3
                and ! array_key_exists( $k, $relatifs1) and ! array_key_exists( $k, $relatifs2))
            {
                $relatifs3[$k] = $a;
            }
       }

       return array_slice( array_merge($relatifs1, $relatifs2, $relatifs3) , 0, 3);
    }


    public function index( Request $request )
    {
        $home = ($request->path() == "/") ? 0 : 1;

        $slider = ($request->input('slider')) ?? 'multy';
        $sliders = $this->prepareItems ( config('sliders.sliders'));

        $filters_1 = config('custom.filters_1');
        $filters_2 = config('custom.filters_2');

        $carousel1 = $this->prepareItems ( config('custom.carousel1'));
        $carousel2 = $this->prepareItems ( config('custom.carousel2'));

        $portfolio1 = $this->prepareItems ( config('custom.portfolio1'));
        
        return view('pages.home')
            ->with('home', $home) 
            ->with('slider', $slider)
            ->with('carousel1', $carousel1)
            ->with('carousel2', $carousel2)
            ->with('portfolio1', $portfolio1)
            ->with('filters_1', $filters_1)
            ->with('filters_2', $filters_2)
            ->with('sliders', $sliders);
    }


    public function slug( Request $request, $slug )
    {
        $articles = $this->prepareItems ( config('custom.articles'), true);
        $article = false;

        foreach( $articles as $key => $a )
        {
            if( $slug == $a->slug )
            {
                if( $key == count($articles)-1 )    $next = 0; else $next = $key + 1;
                if( $key == 0 )                     $prev = count($articles)-1; else $prev = $key - 1;
                $a->prev = $articles[$prev];
                $a->next = $articles[$next];
                $article = $a;
                break;
            }
        }

        if( ! $article ) abort(404);

        $article->relatifs = $this->getRelatifs( $article, $articles );

        return view('pages.articles')
            ->with('article', $article);
    }
}
