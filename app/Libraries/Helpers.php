<?php

//Badge pour date
if (! function_exists('getsharelink'))
{
    function getsharelink( $provider, $url, $text = "" )
    {
       $links = config('GM.sharing');
       if( ! array_key_exists( $provider, $links )) return "#";

       $link = $links[$provider]['uri'].urlencode($url);

       if( $text != "")
       {
        switch ( $provider )
        {
         case "linkedin":
             $link .= "&title=".urlencode($text);
         break;
        }
       }


       return $link;
    }
}
