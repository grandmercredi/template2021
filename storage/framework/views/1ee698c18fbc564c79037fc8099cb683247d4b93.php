<header id="header" data-transparent="true" class="dark">
   <div class="header-inner">
       <div class="container tac">
         <div class="row">

            <div class="col-md-3">
               <a href="#">
                  <img src="/img/logo-tesla-light.png" class="logo-left" alt="Side Logo 1">
               </a>
            </div>
         
            <div class="col-md-6">
               <a href="/">
                  <img src="/img/logo-main-light.png" class="logo-main" alt="Main logo">
               </a>
            </div>
         
            <div class="col-md-3">
               <a href="https://www.grand-mercredi.com/">
                  <img src="/img/logo-gm-light.png" class="logo-right" alt="Side Logo 2">
               </a>
            </div>
         
         </div>
       </div>
  </div>
</header><?php /**PATH /home/vagrant/template/resources/views/layout/headers/three-logo.blade.php ENDPATH**/ ?>