<section id="slider" data-bg-parallax="/img/slider-1.jpg">
    <div class="container tac">

        <div class="wrap-deck left m-t-20">
            <p class="tac" style="margin:auto;">
                <a href="https://grand-mercredi.com" target="_blank">
                    <img src="/img/logo-gm-slider.png" class="logo-gm-slider" alt="Logo GM">
                </a>
                <span class="logo-sep">&</span>
                <a href="https://bit.ly/3l0LNAy" target="_blank">
                    <img src="/img/logo-annonceur.png" class="logo-annonceur-slider" alt="Logo Annonceur">
                </a>
                <br/>
                <span class="introduce">présentent</span>
            </p>
        </div>

        <div class="text-middle text-center">
            
            
            <span data-animate="fadeInUp" data-animate-delay="900" class="btn btn-dark showSlider">See Slider</span>
            <span data-animate="fadeInUp" data-animate-delay="1200" class="btn btn-dark showVideo">See Video-Background</span>
            

        </div>

    </div>
    <div class="scrolldown-animation" id="scroll-down">
        <a class="scroll-to" href="#portfolio">
            <img src="/images/scrolldown-arrow-white.png" alt="Scrolldown Arrow">
        </a>
    </div>
</section><?php /**PATH /home/vagrant/template/resources/views/components/sliders/slider-static.blade.php ENDPATH**/ ?>