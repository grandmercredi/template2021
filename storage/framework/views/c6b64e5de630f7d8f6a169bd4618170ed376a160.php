<section id="carousel" class="model2">
   <div class="container">

      <div class="heading-text heading-line text-center">
         <h4 class="title">Carousel title</h4>
      </div>
      
      <div class="carousel team-members team-members-shadow" data-items="3">
          <?php $__currentLoopData = $carousel2; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $a): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          
          <div class="team-member">
            <h3 style="color:#E41124;min-height: 74px;" class="delius"><?php echo e($a->title); ?></h3>
               <div class="team-image">
                  <a href="<?php echo e($a->link); ?>" target="_blank">
                   <source srcset="<?php echo e(substr($a->img_index,0,-4).".webp"); ?>" type="image/webp">
                   <source srcset="<?php echo e(substr($a->img_index,0,-4).".jpg"); ?>" type="image/jpeg">
                   <img src="<?php echo e($a->img_index); ?>" alt="<?php echo e($a->title); ?>">
                  </a>
               </div>
               <div class="team-desc">
                  <p style="min-height:96px"><?php echo $a->description; ?></p>
                  <a href="<?php echo e($a->link); ?>" class="btn btn-rounded" style="--color:#E41124" target="_blank">En savoir plus</a>
               </div>
          </div>
   
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </div>
   </div>
</section><?php /**PATH /home/vagrant/template/resources/views/components/carousels/model2.blade.php ENDPATH**/ ?>