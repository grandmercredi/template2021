<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-60858895-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-60858895-1');
</script>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="author" content="<?php echo e($article->author ?? 'Grand-Mercredi'); ?>" />
<meta name="description" content="<?php echo e($article->description ?? ''); ?>"/>
<title><?php echo e($article->title ?? ''); ?></title>

 <!-- Favicons-->
 <link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-touch-icon.png">
 <link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png">
 <link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png">
 <link rel="manifest" href="/favicons/site.webmanifest">

 <!-- social graph -->
<meta property="og:title" content="<?php echo e($article->seo_title ?? $article->title ?? ''); ?>"/>
<meta property="og:type" content="<?php echo e($article->seo_type ?? 'website'); ?>"/>
<meta property="og:url" content="<?php echo $article->seo_url ?? url()->current(); ?>"/>
<meta property="og:image" content="<?php echo e(url('/')); ?><?php echo e($article->seo_img ?? $article->img_post ?? $article->img_index ?? '/img/.jpg'); ?>"/>
<meta property="og:locale" content="fr_FR"/>
<meta property="og:description" content="<?php echo e($article->seo_description ?? $article->description ?? ''); ?>"/>
<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="" />
<meta name="twitter:title" content="<?php echo e($article->seo_title ?? $article->title ?? ''); ?>"/>
<meta name="twitter:image" content="<?php echo e($article->seo_img ?? $article->img_post ?? $article->img_index ?? url('/').'/img/.jpg'); ?>"/>
<meta name="twitter:description" content="<?php echo e($article->seo_description ?? $article->description ?? ''); ?>" />

<?php echo $__env->yieldContent('meta'); ?>

<!-- Stylesheets & Fonts -->
<link href="https://fonts.googleapis.com/css?family=Jacques+Francois+Shadow|Poppins|Rozha+One|Montserrat:ital,wght@0,400;0,500;0,700;0,800;0,900;1,700;1,800;1,900&display=swap" rel="stylesheet">

<link href="/css/bootstrap.css" rel="stylesheet">
<link href="/css/theme.css" rel="stylesheet">
<link href="<?php echo e(mix('/css/custom.css')); ?>" rel="stylesheet">


<?php echo $__env->yieldContent('styles'); ?>

<!-- Facebook Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '304886269871050');
    fbq('track', 'PageView');
  </script>
  <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=304886269871050&ev=PageView&noscript=1"
  /></noscript>
  <!-- End Facebook Pixel Code -->

<!-- Video.js -->
<link href="https://vjs.zencdn.net/7.8.4/video-js.css" rel="stylesheet" />

<!-- font-awesome -->
<script src="https://kit.fontawesome.com/f332aca5b4.js" crossorigin="anonymous"></script><?php /**PATH /home/vagrant/template/resources/views/layout/head.blade.php ENDPATH**/ ?>