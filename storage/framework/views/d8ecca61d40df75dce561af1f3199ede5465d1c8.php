<!DOCTYPE html>

<html lang="fr">
<head>
    <?php echo $__env->make('layout.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</head>

<body>
    <!-- Body Inner -->
    <div class="body-inner">
        
        <!--HEADER-->
        <?php echo $__env->yieldContent('header'); ?>
        <!--END: HEADER-->
        
        <!--CONTENT-->
        <?php echo $__env->yieldContent( 'content'); ?>
        <!--END: CONTENT-->
        
        <!-- FOOTER -->
        <?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <!-- end: FOOTER -->
    
    </div>
    <!-- end: Body Inner -->

    <!-- Scroll top -->
    <a id="scrollTop"><i class="icon-chevron-up"></i><i class="icon-chevron-up"></i></a>

    <!--hack IE11-->
    <script src="/js/polyfill.min.js"></script>
    
    <!--Vendor-->
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap.js"></script>

    <!-- Video.js -->
    <script src="https://vjs.zencdn.net/7.8.4/video.js" defer></script>

    <!--Template functions-->
    <script src="/js/theme.js"></script>

    <!--Custom functions-->
    <script src="<?php echo e(mix('/js/custom.js')); ?>" defer></script>    

    <?php echo $__env->yieldContent('scripts'); ?>
</body>
</html>
<?php /**PATH /home/vagrant/template/resources/views//layout/html.blade.php ENDPATH**/ ?>