<section id="carousel" class="model1" style="padding-bottom:0">
   <div class="container-fullscreen">
      <h2 class="title tac m-t-50 m-b-50">
         Carousel title
      </h2>

      <!-- Carousel Filter -->
      <nav class="grid-filter gf-outline tac m-b-60 " data-layout="#portfolio-carousel">
         <ul>
            <?php $__currentLoopData = $filters_1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $f => $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
               <li><a href="#" style="--color:<?php echo e($c); ?>" class="nav-filters" id="nav-filter-<?php echo e($loop->iteration); ?>" data-category=".filter_<?php echo e(Str::slug($f)); ?>"><?php echo e($f); ?></a></li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
         </ul>
      </nav>
      <!-- end: Carousel Filter -->

      <!-- Carousel -->
      <div id="portfolio-carousel" class="grid-layout">
      <?php $__currentLoopData = $carousel1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      
         <div class="portfolio-item <?php $__currentLoopData = $c->tags; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tag): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php echo e("filter_".Str::slug($tag)); ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>" style="background: <?php echo e($c->bg); ?>">
            <h2 class="item-title m-t-50 m-b-50 tac">
               <?php echo e($c->title); ?>

            </h2>
            
            <div class="carousel team-members team-members-shadow container m-t-20" data-items="1">
                
               <?php $__currentLoopData = $c->elem; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $a): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div>
                   <div class="card-carousel position-relative">
                     <div class="container row align">
                        <div class="col-lg-6">
                           <a href="<?php echo e($a['link']); ?>" target="_blank">
                              <source srcset="<?php echo e(substr($a['img_index'],0,-4).".webp"); ?>" type="image/webp">
                              <source srcset="<?php echo e(substr($a['img_index'],0,-4).".jpg"); ?>" type="image/jpeg">
                              <img src="<?php echo e($a['img_index']); ?>">
                           </a>
                        </div>
                        <div class="col-lg-6 position-relative">
                           <div class="align">
                              <h3 style="color: <?php echo e($c->color_1); ?>;font-weight:700;">
                                 <?php echo e($a['title_1']); ?>

                              </h3>
                              <p style="color: <?php echo e($c->color_1); ?>">
                                 <?php echo e($a['text_1']); ?>

                              </p>
                              <div class="card-avis" style="background-color: <?php echo e($c->color_bg); ?>">
                                 <h3 class="kalam" style="color: <?php echo e($c->color_2); ?>;font-weight:700;">
                                    <?php echo e($a['title_2']); ?>

                                 </h3>
                                 <p style="color: <?php echo e($c->color_1); ?>">
                                    <?php echo e($a['text_2']); ?>

                                 </p>
                              </div>
                           </div>

                        </div>
                      </div>
                      <div class="tac hideondesktop-lg m-t-20">
                      <a href="<?php echo e($a['link']); ?>" class="btn btn-univers-2" style="--color: <?php echo e($c->color_1); ?>" target="_blank">en savoir plus</a>
                      </div>
                   </div>
   
                   <div class="tac hideonmobile-lg m-t-20">
                     <a href="<?php echo e($a['link']); ?>" class="btn btn-univers" style="--color: <?php echo e($c->color_1); ?>" target="_blank">en savoir plus</a>
                   </div>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            
            </div>
         </div>
         
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
   </div>
   <!-- end carousel -->

   </div>
</section><?php /**PATH /home/vagrant/template/resources/views/components/carousels/model1.blade.php ENDPATH**/ ?>