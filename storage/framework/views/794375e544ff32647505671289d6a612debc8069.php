<header id="header" data-transparent="true" class="dark submenu-light">
   <div class="header-inner">
       <div class="container">
           <div id="logo">
               <a href="https://www.grand-mercredi.com/">
                  <img src="/img/logo-main-gm-light.png" class="logo-default logo-main" alt="Main Logo">
               </a>
           </div>
           <div id="mainMenu-trigger">
               <a class="lines-button x"><span class="lines"></span></a>
           </div>
           <div id="mainMenu" class="menu-right">
               <div class="container">
                  <nav>
                     <ul>
                         <li class="dropdown"> <a href="#">Menu #1</a>
                             <ul class="dropdown-menu">
                                 <li ><a href="#" data-lightbox="iframe">Catégorie #1</a></li>
                                 <li ><a href="#" data-lightbox="iframe">Catégorie #2</a></li>
                                 <li ><a href="#" data-lightbox="iframe">Catégorie #3</a></li>
                             </ul>
                         </li>
                         <li class="dropdown"> <a href="#">Menu #2</a>
                             <ul class="dropdown-menu">
                                 <li ><a href="#" >Catégorie #1</a></li>
                                 <li ><a href="#" >Catégorie #2</a></li>
                                 <li ><a href="#" >Catégorie #3</a></li>
                             </ul>
                         </li>
                         <li class="dropdown"> <a href="#">Menu #3</a>
                             <ul class="dropdown-menu">
                                 <li ><a href="#" >Catégorie #1</a></li>
                                 <li ><a href="#" >Catégorie #2</a></li>
                                 <li ><a href="#" >Catégorie #3</a></li>
                                 <li ><a href="#" >Catégorie #4</a></li>
                                 <li ><a href="#" >Catégorie #5</a></li>
                             </ul>
                         </li>
                     </ul>
                 </nav>
               </div>
           </div>
       </div>
   </div>
</header><?php /**PATH /home/vagrant/template/resources/views/layout/headers/menu-right.blade.php ENDPATH**/ ?>