<section id="portfolio-carousel">
   <div class="heading-text heading-line text-center">
      <h4 style="font-size:35px;">Carousel</h4>
   </div>
   
   <div class="carousel team-members team-members-shadow container m-t-20" data-items="3">
       <?php $__currentLoopData = $articles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $a): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
       
       <div class="team-member">
           <a href="<?php echo e($a->slug); ?>">
               <div class="team-image">
                   <source srcset="<?php echo e(substr($a->img_index,0,-4).".webp"); ?>" type="image/webp">
                   <source srcset="<?php echo e(substr($a->img_index,0,-4).".jpg"); ?>" type="image/jpeg">
                   <img src="<?php echo e($a->img_index); ?>" alt="<?php echo e($a->title); ?>">
               </div>
               <div class="team-desc">
                   <h3><?php echo e($a->title); ?></h3>
                   <p><?php echo $a->description; ?></p>
               </div>
           </a>
       </div>

       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
   </div>
</section><?php /**PATH /home/vagrant/template/resources/views/components/portfolios/portfolio-carousel.blade.php ENDPATH**/ ?>