<section id="portfolio">
    <div class="container">
        <div class="heading-text heading-line text-center">
            <h4 style="font-size:35px;">Portfolio</h4>
         </div>
         
        <!-- Portfolio Filter -->
        <nav class="grid-filter gf-outline" data-layout="#portfolio-div">
            <ul>
                <li class="active"><a href="#" data-category="*">Tous</a></li>
                <?php $__currentLoopData = $filters; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $f): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li><a href="#" class="<?php echo e($f); ?>" data-category=".filter_<?php echo e(Str::slug($f)); ?>"><?php echo e($f); ?></a></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
            <div class="grid-active-title">Tous</div>

        </nav>
        <!-- end: Portfolio Filter -->

        <!-- Portfolio -->
        <div id="portfolio-div" class="grid-layout portfolio-3-columns" data-margin="20">
            <?php $__currentLoopData = $articles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $a): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php

                if( property_exists($a, 'type') and $a->type=="link")
                        $slug = $a->slug;
                else    $slug = "/".$a->slug;

            ?>
            <!-- portfolio item -->
            <div class="portfolio-item no-overlay img-zoom  <?php $__currentLoopData = $a->tags; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php echo e("filter_".Str::slug($c)); ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>">
                <div class="portfolio-item-wrap">
                    <div class="portfolio-image">
                        <a href="<?php echo e($slug); ?>" target="<?php echo e($a->target ?? ''); ?>">
                            <picture>
                                <source srcset="<?php echo e(substr($a->img_index,0,-4).".webp"); ?>" type="image/webp">
                                <source srcset="<?php echo e(substr($a->img_index,0,-4).".jpg"); ?>" type="image/jpeg">
                                <img src="<?php echo e($a->img_index); ?>" alt="<?php echo e($a->title); ?>">
                            </picture>
                            <?php if(in_array("Videos",$a->tags)): ?>
                                <button type="button" class="btn btn-dark iconovercard bg_kd_red">
                                    <i class="fa fa-play"></i>
                                </button>
                            <?php endif; ?>
                        </a>
                    </div>
                    <div class="portfolio-description">
                        <a href="<?php echo e($slug); ?>" target="<?php echo e($a->target ?? ''); ?>">
                            <h3><?php echo e($a->title); ?></h3>
                            <p><?php echo $a->description; ?></p>
                        </a>
                    </div>
                </div>
            </div>
            <!-- end: portfolio item -->
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        </div>
        <!-- end: Portfolio -->
    </div>
</section>
<?php /**PATH /home/vagrant/template/resources/views/components/portfolios/portfolio.blade.php ENDPATH**/ ?>