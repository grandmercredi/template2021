<section id="portfolio" class="model1">
    <div class="container">

                <h2 class="title tal m-t-50 m-b-50">
                    Portfolio title
                </h2>

                <!-- Portfolio Filter -->
                <nav class="grid-filter gf-outline" data-layout="#portfolio-div">
                    <ul>
                        <?php $__currentLoopData = $filters_2; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $f => $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li><a href="#" style="--color:<?php echo e($c); ?>" class="nav-filters" id="nav-filter-<?php echo e($loop->iteration); ?>" data-category=".filter_<?php echo e(Str::slug($f)); ?>"><?php echo e($f); ?></a></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                    <div class="grid-active-title"><?php echo e(array_key_first($filters_2)); ?></div>
        
                </nav>
                <!-- end: Portfolio Filter -->
        
                <!-- Portfolio -->
                <div id="portfolio-div" class="grid-layout portfolio-2-columns" data-margin="50">
                    <?php $__currentLoopData = $portfolio1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $a): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <!-- portfolio item -->
                    <div class="portfolio-item no-overlay m-b-80 <?php $__currentLoopData = $a->tags; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php echo e("filter_".Str::slug($c)); ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>">
                        <div class="portfolio-item-wrap">
                            <div class="portfolio-image home">
                                <p style="--color:<?php echo e($a->color1); ?>" class="text-border m-img tal title-width"><?php echo $a->title; ?></p>
                                <a href="<?php echo e($a->link); ?>" <?php if(isset($a->target)): ?> target="_blank" <?php endif; ?>>
                                    <img style="--color:<?php echo e($a->color2); ?>" class="portfolio-img-width shadow" src="<?php echo e($a->img_index); ?>">
                                </a>
                            </div>
                            <div class="square tal" style="--color:<?php echo e($a->color2); ?>">
                                <a href="<?php echo e($a->link); ?>" style="--color:<?php echo e($a->color1); ?>" class="btn" <?php if(isset($a->target)): ?> target="_blank" <?php endif; ?>><?php echo $a->title; ?></a>
                                <p><?php echo e($a->description); ?></p>
                            </div>
                        </div>
                    </div>
                    <!-- end: portfolio item -->
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        
                </div>
                <!-- end: Portfolio -->
        </div>
    <img class="yellow-vie-img hideonmobile" src="/img/vdf-yellow.png">
</section><?php /**PATH /home/vagrant/template/resources/views/components/portfolios/model1.blade.php ENDPATH**/ ?>