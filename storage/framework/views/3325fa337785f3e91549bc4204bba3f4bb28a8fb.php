<section id="slider" class="fullscreen" data-bg-parallax="/img/slider-bg.jpg">
    <div class="container tac">

        <div class="wrap-deck l-side">
            <p class="tac" style="margin:auto;">
                <a href="https://grand-mercredi.com" target="_blank">
                    <img src="/img/logo-gm-slider.png" class="logo-gm-slider" alt="Logo GM">
                </a>
                <span class="logo-sep">&</span>
                <a href="https://bit.ly/3l0LNAy" target="_blank">
                    <img src="/img/logo-annonceur.png" class="logo-annonceur-slider" alt="Logo Annonceur">
                </a>
                <br/>
                <span class="introduce">présentent</span>
            </p>
        </div>

        <img src="/img/logo-ope-slider.png" class="img-fluid hideonmobile-lg" alt="Logo Opé Desktop">
        <img src="/img/logo-ope-slider-mob.png" class="img-fluid hideondesktop-lg" alt="Logo Opé Mobile">
        
        
        <div class="m-t-50">
                          
            <a href="/?slider=imgBackground" data-animate="fadeInUp" data-animate-delay="1200" class="btn btn-rounded" style="--color:#e41124">Slider imgBackground</a>
            <a href="/?slider=multy" data-animate="fadeInUp" data-animate-delay="1500" class="btn btn-square" style="--color:#7fa9d1">Slider Multy</a>
            <a href="/?slider=video" data-animate="fadeInUp" data-animate-delay="1800" class="btn btn-corner-rounded" style="--color:#075a4a">Slider Video</a>
            
        </div>

    </div>
    <div class="scrolldown-animation" id="scroll-down">
        <a class="scroll-to" href="#manifesto">
            <img src="/images/scrolldown-arrow.png" alt="Scrolldown Arrow">
        </a>
    </div>
</section><?php /**PATH /home/vagrant/template/resources/views/components/sliders/slider-parallax.blade.php ENDPATH**/ ?>