<div class="post-navigation m-t-40 m-b-20">
    <a href="/<?php echo e($article->prev->slug); ?>" class="post-prev">
        <div class="post-prev-title"><span>Article précédent</span><?php echo e($article->prev->title); ?></div>
    </a>
    <a href="/#articles" class="post-all"><span class="gotohome">Accueil</span><div class="icon-holder"><i class="fa fa-home"></i><span class="text-mutted fz12"></span></div></a>
    <a href="/<?php echo e($article->next->slug); ?>" class="post-next">
        <div class="post-next-title"><span>Article suivant</span><?php echo e($article->next->title); ?></div>
    </a>
</div>
<?php /**PATH /home/vagrant/template/resources/views/components/navigationBottomPost.blade.php ENDPATH**/ ?>