<div id="slider" class="inspiro-slider slider-fullscreen position-relative">
   
    <div class="wrap-deck t-side">
        <p class="tac" style="margin:auto;">
            <a href="https://grand-mercredi.com" target="_blank">
                <img src="/img/logo-gm-slider.png" class="logo-gm-slider" alt="Logo GM">
            </a>
            <span class="logo-sep">&</span>
            <a href="https://bit.ly/3l0LNAy" target="_blank">
                <img src="/img/logo-annonceur.png" class="logo-annonceur-slider" alt="Logo Annonceur">
            </a>
            <br/>
            <span class="introduce">présentent</span>
        </p>
    </div>

    <div class="container tac">
        <img src="/img/logo-ope-slider.png" class="img-fluid hideonmobile-lg align-vertical" alt="Logo Opé Desktop">
        <img src="/img/logo-ope-slider-mob.png" class="img-fluid hideondesktop-lg align-vertical" alt="Logo Opé Mobile">
    </div>

   <?php $__currentLoopData = $sliders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $a): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
   <div class="slide kenburns" style="background-image:url('<?php echo e($a->img_bg); ?>');">
       <div class="bg-overlay"></div>
       <div class="container">
           <div class="slide-captions text-center m-t-100">
                

                              
               <span data-animate="fadeInUp" data-animate-delay="1200" class="showStatic btn btn-<?php echo e($a->color); ?>">See image-background</span>
               <span data-animate="fadeInUp" data-animate-delay="1500" class="showVideo btn btn-<?php echo e($a->color); ?>">See video-background</span>
                

           </div>
       </div>

    </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

    <div class="scrolldown-animation" id="scroll-down">
        <a class="scroll-to" href="#portfolio">
            <img src="/images/scrolldown-arrow-white.png" alt="Scrolldown Arrow" style="width:16px;height:69px;">
        </a>
    </div>

</div>
<?php /**PATH /home/vagrant/template/resources/views/components/sliders/slider.blade.php ENDPATH**/ ?>