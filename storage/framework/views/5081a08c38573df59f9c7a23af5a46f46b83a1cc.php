<header id="header" data-transparent="true" class="dark header-logo-center">
    <div class="header-inner">
        <div class="container">
           <div id="logo">
               <a href="https://www.grand-mercredi.com/" class="m-t-5">
                   <img src="/img/logo-main-gm-light.png" class="logo-default logo-main" alt="Main Logo">
               </a>
           </div>
        </div>
   </div>
</header><?php /**PATH /home/vagrant/template/resources/views/layout/headers/logo-center.blade.php ENDPATH**/ ?>