<?php $__env->startSection('content'); ?>
   <section id="article" class="">
      <div class="container">

         <!-- Post -->
         <div id="blog" class="single-post col-lg-11 center">

            <!-- Post single item-->
            <div class="post-item">
               <div class="post-item-wrap">

                  <?php if( isset($article->video) and $article->video ): ?>
                     <div class="post-video">
                        <iframe width="560" height="315" src="<?php echo e($article->video); ?>" frameborder="0" allowfullscreen></iframe>
                     </div>

                  <?php elseif( isset($article->img_post)): ?>
                     <div class="post-image">
                        <picture>
                           <source srcset="<?php echo e(substr($article->img_post,0,-4).".webp"); ?>" type="image/webp">
                           <source srcset="<?php echo e(substr($article->img_post,0,-4).".jpg"); ?>" type="image/jpeg">
                           <img src="<?php echo e($article->img_post); ?>" alt="<?php echo $article->title; ?>">
                        </picture>
                     </div>
                  <?php endif; ?>

                  <div class="post-item-description">
                     <h1><?php echo $article->title; ?></h1>

                     <div class="post-meta m-b-40 ">
                        <span class="post-meta-description">
                           <?php echo $article->description; ?>

                        </span>
                        <span class="post-meta-share">
                           <?php echo $__env->make('components.articleShare', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                        </span>
                     </div>
                     
                     <section id="article-content">
                        <?php if( isset($article->view) and $article->view ): ?> <?php echo $__env->make( "pages.articles.".$article->view , \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> <?php endif; ?>
                     </section>
                  </div>

                  <?php if( ! isset ($ajax) or ! $ajax): ?>  <?php echo $__env->make('components.navigationBottomPost', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> <?php endif; ?>
               </div>
                <!-- end: Post single item-->

            </div>
            <!-- articles relatifs -->
            <!-- fin articles relatifs -->
         </div>
         <!-- end: blog -->
      </div>
   </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make(config('ZT.theme_views').'.layout.html', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/vagrant/template/resources/views/pages/articles.blade.php ENDPATH**/ ?>