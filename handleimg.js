// -------------------------------------------------------------------------------------
// Kelio
// Script qui compresse les png, jpg et gif en mode lossless ( pas de perte de qualité )
// puis qui génére les webp correspondant au jpg pour l'utilisation de la balise picture
// -------------------------------------------------------------------------------------

// npm install imagemin imagemin-webp imagemin-jpegtran imagemin-optipng imagemin-gifsicle

const imagemin = require("imagemin");

const jpegtran = require('imagemin-jpegtran');
const optipng = require('imagemin-optipng');
const gifsicle = require('imagemin-gifsicle');

const webp = require("imagemin-webp");

outputFolder = "./resources/assets/img_optim/";

PNGImages = "./resources/assets/img/*.png";
JPEGImages = "./resources/assets/img/*.jpg";
GIFImages = "./resources/assets/img/*.gif";


(async () => {
    await imagemin([JPEGImages],{ destination:outputFolder, plugins: [ jpegtran({ quality: 60 }) ] })
    .catch(Error => {
      console.log(Error);        
     console.log(files);
     });

    await imagemin([PNGImages], { destination:outputFolder, plugins:  [ optipng({ optimizationLevel: 5 }) ] })
    .catch(Error => {
      console.log(Error);        
     console.log(files);
     });

    await imagemin([GIFImages], { destination:outputFolder, plugins:  [ gifsicle({ interlaced: true }) ] })
       .catch(Error => {
         console.log(Error);        
        console.log(files);
        });
})();


(async () => {
    await imagemin([JPEGImages],{ destination:outputFolder, plugins: [ webp({ quality: 60 }) ] })
    .catch(Error => console.log(Error));
  //await imagemin([PNGImages],{ destination:outputFolder, plugins: [ webp({ quality: 60 }) ] });
  //await imagemin([GIFImages],{ destination:outputFolder, plugins: [ webp({ quality: 60 }) ] });
})();
